package com.fah.week12;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Collections;
import java.util.HashSet;

public class TestHashSet {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("A1");
        set.add("A2");
        set.add("A3");
        printSet(set);
        System.out.println("----------------");
        set.add("A1");
        printSet(set);
        System.out.println(set.contains("A2"));
        set.remove("A1");
        System.out.println(set);
        System.out.println("----------------");
        HashSet<Integer> set2 = new HashSet<>();
        set2.add(10);
        set2.add(20);
        set2.add(30);
        System.out.println(set2);
        set2.clear();
        System.out.println(set2);

        HashSet<String> set3 = new HashSet<>();
        set3.addAll(set);
        System.out.println(set3);
        
    }

    public static void printSet(HashSet<String> set) {
        Iterator<String> iter = set.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }
}
